FROM python:3.7-alpine3.11
LABEL author="Justin Millette"
ARG IP=3.93.156.9 
COPY * /weather-app/
RUN pip3 install --user -r /weather-app/requirements.txt
CMD python /weather-app/weather_k8s.py $IP
