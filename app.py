from flask import Flask
import weather_k8s

app = Flask(__name__)


@app.route("/weather")
def call_weather():
    return weather_k8s


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)
